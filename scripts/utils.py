import pandas as pd
import scipy
import numpy as np
import matplotlib.pyplot as plt
from sklearn import linear_model, datasets
from sklearn.feature_extraction import DictVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer
from scipy.sparse import hstack, csr_matrix

def fetch_text_features():
    t_data = []
    with open("data/small_paper_text.txt") as fp:
        t_data += [l.strip() for l in fp.readlines()]
    vectorizer = TfidfVectorizer(sublinear_tf=True, max_df=0.5, stop_words='english',
                                 max_features=1000)
    return vectorizer.fit_transform(t_data), vectorizer.get_feature_names()

def fetch_citation_data(use_text_features=True):
    df = pd.read_csv('data/small_paper_features.csv', sep='; ')

    #cols = ['pc', 'cn', 'hi', 'pi', 'conference', 'aff']
    cols = df.columns[:-1]
    print (cols)
    data = [{c: d[c] for c in cols} for _, d in df.iterrows()]

    vectorizer = DictVectorizer()
    sp_matr = vectorizer.fit_transform(data)
    sp_matr[sp_matr!=sp_matr] = 0
    fns = np.asarray(vectorizer.get_feature_names())
   
    if use_text_features:
        tf, _ = fetch_text_features()
        fns = np.concatenate([fns, np.array(_)])
        _d = hstack([sp_matr, tf]).tocsr()
    else:
        _d = sp_matr
        
    _l = int(len(df)*0.7)
    all_Y = df[['tgt']].values.reshape([len(df)])
    def class_code(_):
        if _==0:
            return 0
        elif _>0 and _<10:
            return 1
        elif _<100:
            return 2
        else:
            return 3
        
    all_Y = [_y for _y in map(class_code, all_Y)]
    train_Y = all_Y[:_l]
    test_Y = all_Y[_l:]

    train_X, test_X = _d[:_l], _d[_l:]
    return ({"data": train_X, "target":train_Y}, {"data": test_X, "target": test_Y}, fns)
