import pandas as pd
import scipy
import numpy as np
import matplotlib.pyplot as plt
from sklearn import linear_model, datasets
from sklean.feature_extraction import DictVectorizer

def csr_matrix(data):
    scipy.sparse.csr_matrix(data)

def fetch_citation_data():
    data = pd.read_csv('data/small_paper_features.csv', sep = '; ')

    # X = data[['pc','cn', 'hi', 'pi' ,'conference','aff']]
    # Y = data[['tgt']]
    data.loc[(data['tgt'] > 0) & (data['tgt'] <11)] = 1
    data.loc[(data['tgt'] > 10) & (data['tgt'] <101)] = 2
    data.loc[(data['tgt'] > 100)] = 3
    _l = int(len(data)*0.7)
    train_Y = data[['tgt']][0:_l]
    test_Y = data[['tgt']][_l:]
        
    cols = data.columns
    data = [{cols[i]: _c for i, _c in enumerate(d)} for d in data]
    train_X = data[0:_l]
    test_X = data[_l:]

    vectorizer = DictVectorizer()
    sp_matr_train = vectorizer.fit_transform(train_X)
    sp_matr_test = vectorizer.fit_transform(test_X)
    # #append text feature tf idf
    # fetch_text_features()
    # return ()

    #get text data
    #hstack

fetch_citation_data()
def fetch_text_features():
    pass
#     with open("")
#     v = TFIDFVectorizer()
#     v.fit_transform()
