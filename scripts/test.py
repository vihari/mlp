import pickle
import numpy as np

def closest(v, ee):
    return np.argmin(np.sum((ee-v)**2, axis=1))

def test():
    s = pickle.load(open("res/embs.ser"))
    ee, re = s['ee'], s['re']
    corr = {}
    num = {}
    with open("/home/vihari/repos/mlp/data/cite_kb/test2id.txt") as fp:
        fp.readline()
        while True:
            line = fp.readline()
            if line=='':
                break
            s, o, r = map(int, line.split(" "))
            if closest(ee[s] + re[o], ee) == r:
                corr[o] = corr.get(o, 0) + 1.
            num[o] = num.get(o, 0) + 1.
                
    acc = 0
    for k in corr.keys():
        _ = corr[k]/num[o]
        acc += _
        print ("Class %d Acc: %f" % (k, _))
    acc /= len(num.keys())
    print ("Average accuracy: %f" % acc)

if __name__=='__main__':
    test()
