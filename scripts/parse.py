# parses the files in aminer and emits the files containing features
#rom sklearn.linear_model import LinearRegression
import sys
import numpy as np
from graph_tool.all import *
import matplotlib

DF = "data/"
#PAPERS_FILE, AUTHORS_FILE = DF + "aminernetwork/AMiner-Paper.txt", DF + "aminernetwork/AMiner-Author.txt"
PREFIX = "test"
PAPERS_FILE, AUTHORS_FILE = "data/aminernetwork/test.txt", "data/aminernetwork/testa.txt"
def read_block(fp):
    _ls = []
    while True:
        _l = fp.readline().strip()
        if _l=='':
            break
        _ls.append(_l.strip())
    return _ls

class Paper:
    def __init__(self, _id):
        self.uid = _id
        self.c = 0

    def add_citation(self):
        self.c += 1

    # fill 
    def emit_features(self, _meta):
        # meta should contain the affs vocab, confernces vocab, features for the author
        # all the symbols are one-hot encoded
        # emit freatures of the first author in place of the author
        # [pc, cn, hi, pi] [time, conference, aff] [target=#citations]
        
        author_info = _meta[-1]
        _a = self.authors[-1]
        _ = []
        if _a in author_info:
            _v = author_info[_a]
            _ += _v
        else:
            _ += [0]*4
            
        return _ + [self.where, self.affs[0], self.c]

    @staticmethod
    def initialize(_pb):
        uid = _pb[0].split(' ')[1]
        p = Paper(uid)
        p.title = _pb[1][3:]
        p.authors = _pb[2][3:].split(';')
        p.affs = _pb[3][3:].split(';')
        p.when = _pb[4][3:]
        p.where = _pb[5][3:]
        _ = [l[3:] for l in _pb if l.startswith('#!')]
        if len(_)>0:
            p._abstract = _[0]
        refs = [l[3:] for l in _pb if l.startswith('#%')]
        return p, refs

    @property
    def abstract(self):
        if hasattr(self, "_abstract"):
            return self._abstract
        else:
            return ""
    
def parse_paper():
    path = PAPERS_FILE
    sys.stderr.write('Reading Papers dataset\n')
    papers = {}
    lno = 0
    with open(path, "r") as fp:
        _m = 0 
        while True:
            _ls = read_block(fp)
            if len(_ls) == 0:
                break
            lno += len(_ls)
            if lno%1000==0:
                sys.stdout.write("\rRead %d lines" % lno)
                sys.stdout.flush()

            try:
                _y = int(_ls[4][3:])
            except ValueError:
                continue
            if _y>=2012:
                p, refs = Paper.initialize(_ls)
                papers[p.uid] = p

                for r in refs:
                    if r in papers:
                        papers[r].add_citation()
                        _m += 1

    sys.stdout.write('\n')
    sys.stdout.write('Found %d citations\n' % _m)
    """
    c_v = {p.where:i for i, p in enumerate(papers.values())}
    aff_set = set([aff for p in papers.values() for aff in p.affs])
    author_set = set([auth for p in papers.values() for auth in p.authors])
    aff_v = {aff:i for i, aff in enumerate(aff_set)}
    auth_v = {a:i for i, a in enumerate(author_set)}
    """
    return papers.values()

def parse_author():
    path = AUTHORS_FILE
    sys.stderr.write('Reading Author dataset\n')
    auth = {}
    with open(path,'r') as fp:
        lno = 0
        while True:
            _ls = read_block(fp)
            if len(_ls) == 0:
                break
            lno += len(_ls)
            if lno%1000==0:
                sys.stdout.write("\rRead %d lines" % lno)
                sys.stdout.flush()
                
            if _ls[1].startswith("#n "):
                key = _ls[1][3:].strip()
                val = [0]*4
                for i in range(3, 7):
                    if _ls[i].startswith('#pc'):
                        val[0] = _ls[i][3:].strip()
                    elif _ls[i].startswith('#cn'):
                        val[1] = _ls[i][3:].strip()
                    elif _ls[i].startswith('#hi'):
                        val[2] = _ls[i][3:].strip()
                    elif _ls[i].startswith('#pi'):
                        val[3] = _ls[i][3:].strip()
                # val = [_ls[i][3:].strip() ] # pc,cn,hi,pi
                auth[key] = val
    sys.stdout.write('\n')
    return auth

def plot_graph(_): #
    g_path="citation_network.xml.gz"
    g = load_graph(g_path)

    _t = {'author': 5, 'conference':1 , 'institute':2, 'publication': 6, 'value': 8}
    vp = g.vertex_properties["type"]
    vtype = [_t[vp[v]] for v in g.vertices()]
    nvp = g.new_vertex_property('float', vals=vtype)
    
    pos = sfdp_layout(g)
    graph_draw(g, pos, output_size=(1000, 1000), vertex_color=[1,1,1,0],
               vertex_fill_color=nvp, vertex_size=5, edge_pen_width=0.5,
               vcmap=matplotlib.cm.Dark2, output="cite.png")

def write_triples(g):
    global DF
    DF += "cite_kb/"
    #g = load_graph(g_path)

    vp = g.vertex_properties["label"]
    ep = g.edge_properties["label"]
    n2ids = {g.vertex(i):i for i in range(g.num_vertices())}

    #entity2ids.txt
    with open(DF + "entity2id.txt", "w") as fp:
        fp.write("%d\n" % g.num_vertices())
        for i, v in enumerate(g.vertices()):
            fp.write("%s\t%d\n" % (vp[v], n2ids[v]))

    #relation2ids.txt
    rels = list(set([ep[e] for e in g.edges()]))
    rel2ids = {rels[i]:i for i in range(len(rels))}
    with open(DF + "relation2id.txt", "w") as fp:
        fp.write("%d\n" % len(rel2ids))
        for k, v in rel2ids.items():
            fp.write("%s\t%d\n" % (k, v))

    c_edges = [e for e in g.edges() if ep[e]=='/rel/cite_count']
    nc_edges = [e for e in g.edges() if ep[e]!='/rel/cite_count']
    _l1, _l2 = int(.65*len(c_edges)), int(.8*len(c_edges))
    train, valid, test = c_edges[:_l1]+nc_edges, c_edges[_l1:_l2], c_edges[_l2:]
    
    #triple2id.txt
    with open(DF + "triple2id.txt", "w") as fp:
        fp.write("%d\n" % len(train))
        for e in train:
            rel_label = ep[e]
            sv, tv = e.source(), e.target()
            fp.write("%d %d %d\n" % (n2ids[sv], n2ids[tv], rel2ids[rel_label]))

    #test.txt
    with open(DF + "test2id.txt", "w") as fp:
        fp.write("%d\n" % len(test))
        for e in test:
            rel_label = ep[e]
            sv, tv = e.source(), e.target()
            fp.write("%d %d %d\n" % (n2ids[sv], n2ids[tv], rel2ids[rel_label]))

    #valid.txt
    with open(DF + "valid2id.txt", "w") as fp:
        fp.write("%d\n" % len(valid))
        for e in valid:
            rel_label = ep[e]
            sv, tv = e.source(), e.target()
            fp.write("%d %d %d\n" % (n2ids[sv], n2ids[tv], rel2ids[rel_label]))
            
def write_kb2e_data(papers, authors):
    # code all author indices
    a_i = [[float(_a[i]) for _a in authors.values()] for i in range(4)]
    # get quantiles and emit labels
    ranges = [list(set(np.percentile(_x, [25, 50, 75]))) for _x in a_i]
    prefixes = ['pc', 'cn', 'hi', 'pi', 'cite']
    def code(_v, _type):
        _v = float(_v)
        ti = prefixes.index(_type)
        _range = ranges[ti]
        for i, _r in enumerate(_range):
            if _v<=_r:
                return "/%s/%d" % (prefixes[ti], i) 
        return "/%s/%d" % (prefixes[ti], i+1)

    # for citation
    ranges += [[0, 10, 100]]
    g = Graph()
    v_label = g.new_vertex_property("string")
    v_type = g.new_vertex_property("string")
    e_label = g.new_edge_property("string")

    global cnt
    cnt = 0
    node_list = {}
    def add_node(_t, _id=None):
        global cnt
        v = g.add_vertex()
        if _id is None:
            _id = "/%s/%d" % (_t, cnt)

        node_list[_id] = v
        v_label[v] = _id
        v_type[v] = _t
        cnt += 1
        return v

    # add nodes corresponding to the bins of values
    for i in range(len(ranges)):
        for j in range(len(ranges[i])):
            c = code(ranges[i][j], _type=prefixes[i])
            add_node(_id=c, _t='value')
    
    def insert_author(aname, auth_info):
        av = add_node('author', _id=aname)
        for i in range(4):
            _c = code(auth_info[i], prefixes[i])
            cv = node_list[c]
            a_c = g.add_edge(av, cv)
            e_label[a_c] = prefixes[i]
        return av
    
    for p in papers:
        pv = add_node('publication')
        avs = []
        for author in p.authors:
            if author not in authors:
                continue

            ao = authors[author]
            if author not in node_list:
                av = insert_author(author, ao)
            else:
                av = node_list[author]
        
            p_a = g.add_edge(pv, av)
            a_p = g.add_edge(av, pv)
            
            e_label[p_a] = '/rel/authored_by'
            e_label[a_p] = '/rel/authored'

        conf = p.where
        if conf not in node_list:
            c_node = add_node(_t='conference', _id=conf)
        else:
            node_list[conf]

        p_c = g.add_edge(pv, c_node)
        e_label[p_c] = '/rel/published_at'

        affs = p.affs
        for aff in affs:
            if aff not in node_list:
                iv = add_node(_t='institute', _id=aff)
            else:
                iv = node_list[aff]
        
            p_i = g.add_edge(pv, iv)
            i_p = g.add_edge(iv, pv)
            
            e_label[p_i] = '/rel/affiliation'
            e_label[i_p] = '/rel/produced'

        cite_code = code(p.c, _type=prefixes[-1])
        cv = node_list[cite_code]
        p_c = g.add_edge(pv, cv)
        e_label[p_c] = '/rel/cite_count'
    g.vertex_properties['label'] = v_label
    g.vertex_properties['type'] = v_type
    g.edge_properties['label'] = e_label

    g.save('citation_network.xml.gz')
    plot_graph(g)
    write_triples(g)
    
def run():
    papers = parse_paper()
    authors_meta = parse_author()
    fs = [p.emit_features([authors_meta]) for p in papers]
    # feature dump
    with open("data/%s_paper_features.csv" % PREFIX, "w") as fp:
        # [pc, cn, hi, pi] [time, conference, aff] [target=#citations]
        fp.write('pc; cn; hi; pi; conference; aff; tgt\n')
        for _f in fs:
            _f = map(lambda x: str(x).replace(';', ':'), _f)
            fp.write("; ".join(map(str, _f)) + '\n')

    with open("data/%s_paper_text.txt" % PREFIX, "w") as fp:
        [fp.write(". ".join([paper.title, paper.abstract])+"\n") for paper in papers]
            
    write_kb2e_data(papers, authors_meta)
   
    #fp = pd.read_csv("data/paper_features.csv", sep='; ')
    #print(fp)
    

if __name__=='__main__':
    plot_graph(None)#run()

    
